//AustionIoT ESP8266 MQTT Quickstart Step 2: Get WiFi Connected
//https://bitbucket.org/uCHobby/esp8266_mqtt_quickstart

#include <ESP8266WiFi.h>    //Library for the ESP8266 Wifi functions 

//Flags used to keep track of connection status
bool netConnected=false;		//Flag for network connection.  WiFi etc...

//Edit for your home network
char* wifiSSID="Your SSID";			//WiFi SSID.  **CHANGE THIS**
char* wifiPassword="Your PASSWORD";	//Password for WiFi  **CHANGE THIS**

void setup() {		//Arduino setup function, runs once at reset
	Serial.begin(115200);	//Set the Serial up for 115200 BPS
	delay(500);				//Wait a bit 
	Serial.println("\r\n\r\n");  //Print a few blank lines to get past junk
	Serial.println("ESP8266 MQTT Quick Start");  //Say hello over the terminal
	
	//Get connected to WiFi
	Serial.print("Connecting to ");		
	Serial.println(wifiSSID);
	WiFi.begin(wifiSSID, wifiPassword);	//Start up the WiFi connection
}

void loop() {  		//Arduino loop function, runs over and over...
	//WiFi connection status
	if(WiFi.status()==WL_CONNECTED) {  			//Connected to WiFi?
		if(netConnected){  						//Still connected?  No change
		}
		else {									//New WiFi Connection
			Serial.println("Connected WiFi!");
		}
		netConnected=true;						//Flag Connected
	}
	else { 										//Not connected to WiFi
		if(netConnected) { 						//Lost connection?
			Serial.println("Disconnected WiFi");
		}
		else {									//Still not connected, no change
		}
		netConnected=false;						//Flag Not Connected
	}
}
