//AustionIoT ESP8266 MQTT Quickstart Step 3: Get MQTT Connected
//https://bitbucket.org/uCHobby/esp8266_mqtt_quickstart

#include <ESP8266WiFi.h>    //Library for the ESP to work with Arduino 
#include <PubSubClient.h>   //Library for MQTT connections

//Flags used to keep track of connection status
bool netConnected=false;		//Flag for network connection.  WiFi etc...
bool mqttConnected=false;		//Flag for MQTT connection.

//**CHANGE THIS**
char* mqttID= "Your MQTT ID";   	//ID used for MQTT connection, must be unique across everything, one per connection  **CHANGE THIS**
char* wifiSSID="Your SSID";			//WiFi SSID.  **CHANGE THIS**
char* wifiPassword="Your PASSWORD";	//Password for WiFi  **CHANGE THIS**
const char* mqttServer = "Your MQTT Broker";	//Address for the MQTT broker **CHANGE THIS**

const int mqttPort=1883; 					//Port for MQTT connection

WiFiClient espClient;			//Adapter for network services used by PubSubClient

PubSubClient mqttClient( mqttServer,mqttPort,espClient);  //Adapter for MQTT Broker

void setup() {		//Arduino setup function, runs once at reset
	Serial.begin(115200);	//Set the Serial up for 115200 BPS
	delay(500);				//Wait a bit 
	Serial.println("\r\n\r\n");  //Print a few blank lines to get past junk
	Serial.println("ESP8266 MQTT Quick Start");  //Say hello over the terminal
	
	//Get connected to WiFi
	Serial.print("Connecting to ");
	Serial.println(wifiSSID);	
	WiFi.begin(wifiSSID, wifiPassword);	//Start up the WiFi connection
}

void loop() {  		//Arduino loop function, runs over and over...
	//WiFi connection status
	if(WiFi.status()==WL_CONNECTED) {  			//Connected to WiFi?
		if(netConnected){  						//Still connected?  No change
		}
		else {									//New WiFi Connection
			Serial.println("Connected WiFi!");
			Serial.println("MQTT: Connecting");
			mqttClient.connect(mqttID);			//Connect to MQTT broker now that we have a connection to the net.
		}
		netConnected=true;						//Flag Connected
	}
	else { 										//Not connected to WiFi
		if(netConnected) { 						//Lost connection?
			Serial.println("Disconnected WiFi");
		}
		else {									//Still not connected, no change
		}
		netConnected=false;						//Flag Not Connected
	}

	//MQTT connection status
	if(mqttClient.connected()) {  				//Connected to MQTT
		if(mqttConnected) {  					//Still connected to MQTT? No change
		}
		else {									//New MQTT Connection
			Serial.println("MQTT Connected");
		}
		mqttConnected=true;						//Flag MQTT Connected
	}
	else {  									//Not connected to MQTT
		if (mqttConnected) {  					//Lost MQTT connection?
			Serial.println("MQTT Disconnected");
		}
		else {									//still not connected to MQTT, No change
		}
		mqttConnected=false;					//Flag MQTT not connected.
	}
	mqttClient.loop();							//Give the MQTT library some processing time CRITICAL!
}
