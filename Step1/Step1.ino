//AustionIoT ESP8266 MQTT Quickstart Step 1: Hello World
//https://bitbucket.org/uCHobby/esp8266_mqtt_quickstart
const int BLUE_LED=16;    //Blue LED on the NodeMCU module
const int FLASH_BUTTON=0; //Flash button is connected to GPIO
const int LED_ON=0;		  //The LED is on when you write 0 to the GPIO
const int LED_OFF=1;	  //LED is off with 1.
const int BUTTON_ON=0;    //Button goes low on press
const int BUTTON_OFF=1;   //Button goes high normally


void setup() {		//Arduino setup function, runs once at reset
	Serial.begin(115200);	//Set the Serial up for 115200 BPS
	delay(500);				//Wait a bit for things to get stable. Probably not necessary. 
	Serial.println("\r\n\r\n");  //Print a few blank lines to get past junk ouput at RESET
	Serial.println("ESP8266 MQTT Quick Start");  //Say hello over the terminal
	pinMode(BLUE_LED,OUTPUT);	//Set the LED pin to output mode.
	pinMode(FLASH_BUTTON,INPUT);//Set the button pin to input.
}

void loop() {  		//Arduino loop function, runs over and over...
	int buttonState=digitalRead(FLASH_BUTTON);  //Read the button input
	if (buttonState==BUTTON_ON) {				//Is the button pressed?
		digitalWrite(BLUE_LED, LED_ON);			//Set the LED on when the button is pressed
	}
	else {										//The button is not pressed
		digitalWrite(BLUE_LED, LED_OFF);			//Turn off the LED
	}
}
