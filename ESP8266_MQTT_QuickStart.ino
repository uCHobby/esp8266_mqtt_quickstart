#include <ESP8266WiFi.h>    //Library for the ESP to work with Arduino 
#include <PubSubClient.h>   //Library for MQTT connections

void callback(char* topic, byte* payload, unsigned int length);  //Callback function for MQTT Messages

//Flags used to keep track of connection status
bool netConnected=false;		//Flag for network connection.  WiFi etc...
bool mqttConnected=false;		//Flag for MQTT connection.

char* mqttID= "uCHobby";   		//ID used for MQTT connection.
char* wifiSSID="GORN";			//WiFi SSID to connect with
char* wifiPassword="2k6a7x7+5j4y";	//Password for WiFi

WiFiClient espClient;			//Adapter for network services used by PubSubClient

const char* mqttServer = "austiniot.com";	//Address for the MQTT broker
const int mqttPort=1883; 					//Port for MQTT connection
PubSubClient mqttClient( mqttServer,mqttPort,espClient);  //Adapter for MQTT Broker

void setup() {		//Arduino setup function, runs once at reset
	Serial.begin(115200);	//Set the Serial up for 115200 BPS
	delay(500);				//Wait a bit 
	Serial.println("\r\n\r\n");  //Print a few blank lines to get past junk
	Serial.println("ESP8266 MQTT Quick Start");  //Say hello over the terminal
	
	//Get connected to WiFi
	Serial.print("Connecting to ");
	Serial.println(wifiSSID);	
	WiFi.begin(wifiSSID, wifiPassword);	//Start up the WiFi connection
	mqttClient.setCallback(callback);	//Set the MQTT callback for when we get messages later.
}

void loop() {  		//Arduino loop function, runs over and over...
	//WiFi connection status
	if(WiFi.status()==WL_CONNECTED) {  			//Connected to WiFi?
		if(netConnected){  						//Still connected?  No change
		}
		else {									//New WiFi Connection
			Serial.println("Connected WiFi!");
			Serial.println("MQTT: Connecting");
			mqttClient.connect(mqttID);			//Connect to MQTT broker now that we have a connection to the net.
		}
		netConnected=true;						//Flag Connected
	}
	else { 										//Not connected to WiFi
		if(netConnected) { 						//Lost connection?
			Serial.println("Disconnected WiFi");
		}
		else {									//Still not connected, no change
		}
		netConnected=false;						//Flag Not Connected
	}

	//MQTT connection status
	if(mqttClient.connected()) {  				//Connected to MQTT
		if(mqttConnected) {  					//Still connected to MQTT? No change
		}
		else {									//New MQTT Connection
			Serial.println("MQTT Connected");
			mqttClient.subscribe("#");			//Subscribe to everything, not reccomended!
			mqttClient.publish("chat", "Hello AustinIoT MQTT!");  //Send a hello to chat topic.
		}
		mqttConnected=true;						//Flag MQTT Connected
	}
	else {  									//Not connected to MQTT
		if (mqttConnected) {  					//Lost MQTT connection?
			Serial.println("MQTT Disconnected");
		}
		else {									//still not connected to MQTT, No change
		}
		mqttConnected=false;					//Flag MQTT not connected.
	}
	mqttClient.loop();							//Give the MQTT library some processing time CRITICAL!
}

//Handle MQTT messages
void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {  //Print all the characters up to the lenght, not null terminated...
    Serial.print((char)payload[i]);
  }
  Serial.println();
}

