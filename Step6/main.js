'use strict';
//Simple javascript example for the AustinIoT MQTT QuickStart
//Find more info at the BitBucket Repo
//https://bitbucket.org/uCHobby/esp8266_mqtt_quickstart
//David Fowler

/* global $ google*/

/*global Messaging */
//MQTT Broker 
var mqttAddress = "Your MQTT Broker"; //**Change This***
var mqttClientID = "Your MQTT Client ID"; //**Change This***  Must be unique across all connections, one id per.
var mqttPort = 8883;
var mqttConnection;

var mqttConnectionOptions = {
    onSuccess: onConnect
};

$(document).ready(main);

function main() {    
    mqttConnection = new Messaging.Client(mqttAddress, mqttPort, mqttClientID);
    mqttConnection.onConnectionLost = onConnectionLost;
    mqttConnection.onMessageArrived = onMessageArrived;
    mqttConnection.connect(mqttConnectionOptions);
}

function onConnect() {
    console.log("onConnect");
    mqttConnection.subscribe("#");
}

function onConnectionLost(responseObject) {
    console.log("onConnectionLost");
    if (responseObject.errorCode !== 0) {
        console.log("  --" + responseObject.errorMessage);
    }
}

function onMessageArrived(message) {
    var payload = message.payloadString;
    var topic = message.destinationName;

    console.log("MQTT RX:"+topic+" : "+payload);
}
