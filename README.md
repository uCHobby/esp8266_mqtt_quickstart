# ESP8266 MQTT QuickStart #

### Step by step guide for getting any ESP8266 based project up and running with MQTT.  
This guide was originally created to support a presentation given at the 
[AustinIoT Hardware Focus Meetup in July of 2016](http://www.meetup.com/AustinIoT/events/232515797/).

The Arduino platform with ESP8266 support is used, but the code should be usable on any Arduino based platform with little to no modifications.

#### What you need
* ESP8266 NodeMCU module (any ESP8266 module should work)
* Arduino IDE with ESP8266 support and libraries installed
* ESP8266WiFi Library
* PubSubClient Library
* An MQTT Broker to work with. (AustinIoT group has one for members)
* Microsoft Code Editor (recommended)

### Who can I talk to about this?
Please contact [David Fowler, AKA uCHobby](http://fowlers.net/index.php/david) or the [Austin IoT group](http://www.meetup.com/AustinIoT/events/232515797/) for more information.
Reference this project, "ESP8266 MQTT QuickStart" in related communications.

### What can I use this code for? ###
You can do anything you like with the code presented here. If you find it useful, mention the orignal source or the Austin IoT group. We also hope for feedback on any code issues or
improvements.

## Guide structure ##
This file is the guide. Source files are grouped by step number in sub-directories.  Step1, Step2, etc...  
Open the code for each step as you follow along with the explanations here, run that code and verify that things work.
Then move on.

The details of how the code functions are provided as comments in the code module and the guided walkthrough at the 
presentation.

# Step0: Get Setup
Verify that your development system is set up and able to program simple code into your ESP8266 module. A lot of things must work for this step to succeed.  Have patience and get good at following instructions you find online. 
### Confirm that you have the following ready before you load the code for Step 1
##### - Installed Arduino IDE (I'm using version 1.6.9)
##### - Installed ESP8266 support
##### - Installed Microsoft Code editor (optional)
##### - Cloned the GIT repo onto your system

ESP8266 support for the Arduino install instruction and documentation can be found at the [ESP8266 Core on Github here](https://github.com/esp8266/Arduino).
For this guide, we use the NodeMCU module which has a UART to USB adapter, power supply, and some buttons to help with programming the module.  You may need to find instructions specific to this module to get past this first step but put
in the effort as it will save you troubleshooting time.

If you get stuck, use the "project-esp8266" channel on the AustinIOT Slack to ask for help.

#### Tip: Setup the Arduino IDE
You will need to set the COM port and board type in the IDE before you can program a module. Under "Tools" set the board type to match your board and the COM port your module is connected at.
#### Tip: Find what COM port your module is using
On Windows use Device Manager, find the "Ports (COM & LPT)" section and watch for the COMX port that
shows when you plug or unplug the USB connection on the module.
On OSX, you can use the "about this Mac" and hunt down the port name in system info

#### Tip: Using an external editor
The Arduino IDE has a very basic text editor, but you are not stuck using it.  You can use any text based editor and Microsoft Code is free and pretty awesome; it's my editor of choice. 
    
Using an external editor means disabling the internal editor in Arduino. Do that in the preferences.
Check the use external editor box. All your code changes in the external editor are automatically updated 
in the IDE.
    
You can now just use the IDE as a build tool and for the serial terminal window.
#### Tip: Programming the module from the IDE 
Before clicking the program button in Arduino, you need to get your ESP into FLASH mode using the two push-buttons on the module, RESET, and FLASH.  Press and hold the FLASH button then tap RESET. Release the FLASH button just after 
the RESET button. Nothing happens now, but you will see a line of "." in the IDE window after you clicks the download button.  Be patient as it could be a bit before the FLASHing starts and you will see a long line of dots before 
it's done.

##### Austin IoT MQTT Broker for Members
The group has it's own MQTT Broker for experiments by its members. Do the following to get access.
 * Get on slack and join the project-esp8266 channel, where you will find support. (Find a group admin to get setup with Slack)
 * Let the admins know what you are doing. (use Slack) 
 * At some point you may need to get an account, for now, it's open.

You can use the group broker for your projects as long as it's personal and not a burden on the free resources.  This server is provided, paid for, by the group leader(s). It's OK to develop a product using the group broker but do 
not put your product into production without approvals.

If you are an Austin IoT group member, agree not to do harm, and not to make money without cutting in 
the group, you are welcome to use the group broker.

Connect to the broker at:
 * Host: AustinIoT.com
 * Port: 1883 (for devices), 8883 for WebSocket applications
 * Client ID: A short but descriptive name, something that lets the admins know who you are.
 * User and Password are not used now but that could change.

# Step1: Hello World 
##### - Open Step1.ino in the ArduinoIDE
##### - Put your module into FLASH mode
##### - Then click the download button. 
This might take a few minutes to compile. Eventually, you should see a chunk of text starting with 
"Sketch uses XXXX bytes..." After that you should see "Uploading...." and a steady progression of "." 
As the program is transferred. Wait till the load is done...

##### - Open the Serial Monitor window (button on right top of IDE), set the baud rate to 115200
You should see "ESP8266 MQTT Quick Start."  If you don't, try tapping the RESET button on your module

##### - Walkthrough
Don't skip this step. It's simple but sets the stage or everything that follows. It this works then we know you have a good starting point. 

Now look at the code, it's commented heavily. If you don't understand every line of code, you should 
stop here till you do.

### Troubleshooting Step 1
The most likely problem here is the serial connection. You might have the wrong port selected. Double check that you have the correct COM port.

It's okay to see some garbage characters on the terminal just after reset, but it should clear up and cleanly print "ESP8266 MQTT Quick Start."

If you have the correct port and the programming is failing, you are probably not going into FLASH mode 
or you have the wrong board selected in the "Tools" menu.

# Step2: Get WiFi Connected
You will need the ESP8266WiFi Library for this step. It should already be installed as part of the 
ESP8266 support. 
##### - Open Step2.ino in the ArduinoIDE
Take a moment to study the code. Take note of the print statements that will show progress as the connection is made to your wifi.
##### - Edit Wifi settings
Find the wifiSSID and wifiPassword line near the top of the file (lines 7 and 8 ATM). Edit these values to match your home network settings. Save the file (ctrl-S)
##### - FLASH the code into your module
Just like in step 1.
##### - Check the Serial Messages
Open the serial terminal and watch the messages. If you don't see the "ESP8266 MQTT Quick Start" text try pressing the RESET button.

If things are right, you should see "Connected WiFi!"  

##### - Walkthrough
Pay close attention to the code in the loop() function where it's checking the WiFi status. Some trickery is used to track the state so we can tell if we got a new connection or lost it etc..

### Troubleshooting Step 2
If you have any trouble loading the program or can't see the "ESP8266 MQTT Quick Start line on the terminal, 
go back to step 1 troubleshooting.

About the only thing that can go wrong here is that it's not finding your WiFi, or your SSID/Password is wrong in the code. Double check the settings. Prove you can connect with those same settings with another device.

It is possible that your module is not compatible with the security settings of your WiFi router. You could try another access point, maybe use your cell phone's hot spot. If nothing works, perhaps your module is broken.

# Step3: Get MQTT Connected
You will need the ESP8266WiFi Library for this step. It should already be installed as part of the 
ESP8266 support. 
##### - Open Step3.ino in the ArduinoIDE
Take a moment to study the code. Take note of the print statements that will show progress as the connection is made to the MQTT Broker.
##### - Edit Wifi settings
Just like in Step 2, find the wifiSSID and wifiPassword line near the top of the file 
(lines 7 and 8 ATM). Edit these values to match your home network settings.
You must also edit the clientID setting to something unique for you. The broker will drop connections that have the same clientiD. Each device will need a clientID which you might derive from the ESP8266 device ID.

##### - Edit the MQTT setting to match your broker
You will also need to edit the MQTT settings to point at your Broker. If you want to use the AustionIoT members MQTT Broker, you will need to check with one of the group admins for the details.
##### - FLASH the code into your module
Just like in step 1.
##### - Check the Serial Messages
Open the serial terminal and watch the messages. If you don't see the "ESP8266 MQTT Quick Start" text try pressing the RESET button.

If things are right, you should see "Connected WiFi!"  Then "Connected to MQTT."
##### - Walkthrough
Step3.ino adds just enough code to get connected to MQTT. You would have already gone through the code to change the MQTT settings but now look at the loop() code for the section where the MQTT status is checked. 
Like the WiFi status code, the state is compared to the prior state to detect initial connections and loss cases.

### Troubleshooting Step 3
If you have any trouble loading the program or can't see the "ESP8266 MQTT Quick Start line on the terminal, 
go back to step 1 troubleshooting.

If you don't see the "Connected WiFi!" line on the terminal, go back to step 2 troubleshooting.

About the only thing that can go wrong here is that it's not finding the MQTT broker. Double check the settings. 

It could be that you're on a network that blocks the necessary ports, an issue if you are doing this on a corporate network. 

It's also possible that the MQTT server is down.

Lastly, you might not have the library loaded, but that should cause a compiler error.

# Step 4: MQTT Hello Chat
You should have everything you need to complete this step. It's a cakewalk from here.  Let's send and receive a message now. 
##### - Open Step4.ino in the ArduinoIDE
Take a moment to study the code. Take note of the print statements that will show progress as the connection is made to the MQTT Broker.
##### - Edit Wifi settings
Just like in Step 2, find the wifiSSID and wifiPassword line near the top of the file (lines 7 and 8 ATM). Edit these values to match your home network settings.
##### - Edit the MQTT setting to match your broker
You will also need to edit the MQTT settings to point at your Broker as you did in step 3. 
##### - FLASH the code into your module
##### - Check the Serial Messages
Open the serial terminal and watch the messages. You should see all the same getting connected messages from previous steps and a few new ones showing that we sent and received messages.

    ESP8266 MQTT Quick Start
    Connecting to GORN
    Connected WiFi!
    MQTT: Connecting
    MQTT Connected
    Message arrived [chat] Hello from David.  Send a message to chat. We are watching
    Message arrived [chat] Hello AustinIoT MQTT!

The "Hello from David" message was sent automatically from the server because that message had been forwarded to the topic "chat" with a retain flag.  The second "Hello AustinIoT" message was sent by the module to the broker and echoed back on the same topic.

##### - Walkthrough
Step4.ino adds just enough code to subscribe to messages and to send one via MQTT. Look for the new code spots where we have a message callback and a subscribe/publish in the loop() function. As before, the code is heavily commented. 

If you understand the code at this point, you are ready to do some neat projects.

### Troubleshooting Step 4
You should not have any new problems here. Go back to previous steps to find help.

# Step 5: Online MQTT Client Tool
In this step, we look at a tool for monitoring and testing with MQTT.
##### - With your Browser, preferably Chrome, open the [HiveMQ Online MQTT Client](http://www.hivemq.com/demos/websocket-client/)
The top of your browser view should include edit boxes for you to add your MQTT Broker details. You should know what to add here. Just enter the top line of items; hostname, port, and a descriptive but short clientID; I use "uCHobby". 
Click the "Connect" button when ready.

##### - Subscribe to a topic
Now that you are connected the view has two major areas, the publish and the subscription area.  The publish area shows messages that are received on a topic you subscribe to in the subscription area.

Click on the "Add new Topic Subscription" button. Set the QOS to 0. Set the topic to "#". Finally, click on the 
"Subscribe" button.

You should see a message from me pop up instantly.  This message is stored at the server because it was sent with the retain flag. Don't worry about this flag, it's an advanced topic for later.  

If you don't see my message, it's possible the server has been restarted, and the message was lost. Don't let it 
stop you, just keep on going with the steps here.

##### - Publish a message to a topic
To publish a message means to send it.  Like an email, a message has a topic (subject) and a body. For this test, 
set the topic to "chat" and put a hello message in the body. "Hello, this is Bill." Click the "publish" button.

You should see your message pop up as being received by the client. You subscribed to topic="#" before which means 
"All messages" so you will receive every message sent through the Broker.  If there is too much message traffic change your subscription so that you subscribe only to the "chat" topic.

# Step 6: Web Application
In this step, we look at using WebSockets to enable web application development with MQTT.

JavaScript running on a web page can also connect to MQTT.  Devices, like the ESP8266, connect to the broker using
basic TCP. This keeps it simple for small devices with limited resources. For web applications, we can connect using WebSockets.  

WebSockets are like a TCP connection but are a bit more constrained and simplified. They work over HTTP and are more complicated to implement.  With a WebSocket connection, you get real-time
two-way communications between the server and the code of your web application.

For the web client side MQTT work we use the [Eclipse Paho JavaScript Client library](https://eclipse.org/paho/clients/js/) 
(named mqttws31.js in the code)

##### - Code Walkthrough
You will find the web application in the Step6 folder.  It's a work in progress right now and might be overly complicated as I've started with an elaborate application under development on another project. I'll revise this text as I get this done.
* index.html - This is the web page that host the application, it loads all the other files into your browser and controls the view you see.
* main.css - Cascading Style Sheet used by the HTML to control the styles used. color, text size, formatting, etc..
* main.js - The JavaScript application main code module.
*mqttws31.js - The Eclipse Paho library used by main.js for broker connection.  

# Appendix - MQTT Topic Filtering
##### '#' character
Represents a complete sub-tree of the hierarchy and must be the last character in a subscription topic string.

"SENSOR/#" will match any topic starting with SENSOR/, such as SENSOR/1/TEMP and SENSOR/2/HUMIDITY.

##### '+' character
Represents a single level of the hierarchy and is used between delimiters. 

For example, SENSOR/+/TEMP will match SENSOR/1/TEMP and SENSOR/2/TEMP.
# Appendix - Extra Information
#### ESP8266 Links
* [ESP8266 Arduino QuickStart](https://bennthomsen.wordpress.com/iot/iot-things/esp8266-wifi-soc/esp8266-getting-started-with-arduino-ide/)
(NodeMCU module pin mapping)
* [ESP8266.net](http://esp8266.net/) (ESP8266 reference Source)
* [Kolban’s book on the ESP8266](http://neilkolban.com/tech/esp8266/)
* [ESP8266 Modules at Amazon](https://www.amazon.com/s/?ie=UTF8&keywords=esp8266&tag=mh0b-20&index=aps&hvadid=4968319533&hvqmt=e&hvbmt=be&hvdev=c&ref=pd_sl_6quvsef11y_e) (Lots of options)

#### MQTT Links
 * [IBM Introduction](http://www.ibm.com/support/knowledgecenter/SSFKSJ_7.5.0/com.ibm.mm.tc.doc/tc00000_.htm)
 * [MQTT reference](http://mqtt.org/)
 * [HiveMQ Online Tools List](http://www.hivemq.com/blog/seven-best-mqtt-client-tools)
 * [My Fav Online MQTT Client](http://www.hivemq.com/demos/websocket-client/)
 * [Mosquitto MQTT Broker](http://mosquitto.org/) (This is what we use at AustinIoT)

#### Related Libraries 
 * [ESP8266 Arduino](https://github.com/esp8266/Arduino)
 * [PubSubClient](https://github.com/knolleary/pubsubclient)

#### Markdown
This guide is formatted in Markdown. I used an online editor called Dillinger to cheat, but you can do the initial editing work in any text editor.  After my first pass, I learned that Microsoft Code has support via a plugin for Markdown, Awesome!

 * [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
 * [Dillinger](http://dillinger.io/)

#### Visual Studio Code
By now you know I am a major fan of this editor too. VS Code is cross-platform and supports a ton of languages.
 * [Visual Studio Code](https://code.visualstudio.com/)